Tool to forward command inputted from PC to Atlas Scientific sensors, through ESP32.

## Use cases
- When we need to calibrate the sensors which are already connected to ESP32 board and it is troublesome to remove the sensors to connect to PC.

## Setup
- Connect the UART0 to PC's USB.
- Connect the sensor to any ESP32 GPIO port.
- Change `TX_SENSOR`, `RX_SENSOR` values (in _config.h_) to ESP32 pins which are connected to the sensor. In one communication, we only do with one sensor.
- Compile the code, using [PlatformIO](https://platformio.org/) toolset, and flash to ESP32.
- Run a serial port communication program, like [CuteCom](https://github.com/neundorf/CuteCom). Note to select "CR" as line end character.

![CuteCom](http://i.imgur.com/tStvCz1.png)

