/**
 * ESP32 application, for using ESP32 as replacement of "USB-to-TTL-Serial" cable,
 * to communicate with Atlas Scientific sensors from PC.
 * Author: Nguyễn Hồng Quân <ng.hong.quan@gmail.com>
 **/

#include <Arduino.h>
#include "config.h"

uint8_t cmd_len = 0;
uint8_t command[MAX_COMMAND_LENGTH] = {0};
uint8_t rsp_len = 0;
uint8_t response[MAX_RESPONSE_LENGTH] = {0};
// Serial to sensor, using UART1
HardwareSerial senseri(1);


void setup() {
    Serial.begin(115200);
    senseri.begin(9600, SERIAL_8N1, RX_SENSOR, TX_SENSOR);
}


void loop() {
    if (senseri.available()) {
        rsp_len = senseri.readBytesUntil('\r', response, MAX_RESPONSE_LENGTH);
    }
    if (rsp_len) {
        // Recover \r, which was skipped by readBytesUntil
        response[rsp_len] = '\r';
        ++rsp_len;
        // Print to PC-end serial
        Serial.write(response, rsp_len);
        Serial.flush();
        rsp_len = 0;
        memset(response, 0, MAX_RESPONSE_LENGTH);
        delay(10);
    }
    if (Serial.available()) {
        cmd_len = Serial.readBytesUntil('\r', command, MAX_COMMAND_LENGTH);
    }
    if (cmd_len) {
        delay(10);
        // Recover \r, which was skipped by readBytesUntil
        command[cmd_len] = '\r';
        ++cmd_len;
        Serial.printf("Received command (len=%d):\n", cmd_len);
        Serial.write(command, cmd_len);
        // Forward command to sensor
        senseri.write(command, cmd_len);
        senseri.flush();
        cmd_len = 0;
        memset(command, 0, MAX_COMMAND_LENGTH);
        delay(10);
    }
}
